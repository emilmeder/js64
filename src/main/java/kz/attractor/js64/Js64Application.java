package kz.attractor.js64;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Js64Application {

    public static void main(String[] args) {
        SpringApplication.run(Js64Application.class, args);
    }

}
