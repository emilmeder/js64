package kz.attractor.js64.controller;

import kz.attractor.js64.model.Task;
import kz.attractor.js64.repository.TaskRepository;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TaskController {

    public final TaskRepository tr;

    public TaskController(TaskRepository tr) {
        this.tr = tr;
    }

    @GetMapping("/task/all")
    public List<Task> findAll(){
        return (List<Task>) tr.findAll();
    }

    @GetMapping("/task/delete")
    public List<Task> delete(){
        tr.deleteAll();
        return (List<Task>) tr.findAll();
    }

    @PostMapping("/task")
    public Task makeNew(@RequestParam("task") String task){
        Task newTask = new Task(task);
        tr.save(newTask);
        return newTask;
    }

    @PostMapping("/task/status")
    public Task change(@RequestParam("id") String id){
        Task task = tr.findTaskById(id);
        if (task.getStatus()){
            task.setStatus(false);
        }

        else {
            task.setStatus(true);
        }
        tr.save(task);
        return task;
    }
}
