package kz.attractor.js64.repository;

import kz.attractor.js64.model.*;
import org.springframework.data.repository.*;

public interface TaskRepository extends CrudRepository<Task, String> {

     public Task findTaskById(String id);
}