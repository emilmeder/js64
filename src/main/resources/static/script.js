'use strict'



class Task {
constructor(id,name,status) {
    this.id = id;
    this.name = name;
    this.status = status;
    }
}

function makeNewTask(task) {
    let element = document.createElement(`li`);
    element.innerHTML = `${task.name}`;

    let newClass = document.createAttribute("id");
    newClass.value = `${task.id}`;
    element.setAttributeNode(newClass);

    let newClass2 = document.createAttribute("ondblclick");
    newClass2.value = `changeStatus(this.id)`;
    element.setAttributeNode(newClass2);
    if (`${task.status}`=="true"){
        console.log("true")
    }

    else{
        let newClass3 = document.createAttribute("class");
        newClass3.value = `status`;
        element.setAttributeNode(newClass3);
    }
    document.getElementById("taskList").appendChild(element);
}

async function findTask() {
    const taskForm = document.getElementById("form");
    const response = await fetch('http://localhost:8080/task/all');
    if (response.ok) {
        let taskJson = await response.json();
        for (let i=0; i<taskJson.length; i++){
        makeNewTask(new Task(
        taskJson[i].id,
        taskJson[i].name,
        taskJson[i].status))
        }
        taskForm.reset();
        document.getElementById("myInput").focus();
    }
    else {
        taskForm.reset();
        document.getElementById("myInput").focus();
    }
}

function f2(){
    const taskForm = document.getElementById("form");
    let data = new FormData(taskForm);

    fetch("http://localhost:8080/task", {
        method: 'POST',
        body: data
    }).


    then(r => {window.location.href = "http://localhost:8080/"});
};



    window.addEventListener("load",async function () {
        await findTask();

    document.getElementById("form").addEventListener("submit",f2);
})